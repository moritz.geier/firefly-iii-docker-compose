# Docker Compose for Firefly-III

This is a simple setup of [Firefly-III](https://github.com/firefly-iii/firefly-iii) using docker-compose.
Before starting up the containers we have to create our SSL Certificates.
```
openssl genrsa -out nginx/certs/firefly-iii.key 2048
openssl req -new -key nginx/certs/firefly-iii.key -out nginx/certs/firefly-iii.csr
openssl x509 -signkey nginx/certs/firefly-iii.key -in nginx/certs/firefly-iii.csr -req -days 365 -out nginx/certs/firefly-iii.crt
```
Fill in the required information. These need to be renewed after a year.

Then we need to configure our database by copying `.db.env.template` to `.db.env` and inserting a strong
database password after the `POSTGRES_PASSWORD` variable.

We also have to do the same for `.app.env.template` and copy it to `.app.env` and insert the same password
that we set for the database as value for `DB_PASSWORD`

Copy `.importer.env.template` to  `.importer.env` and set `VANITY_URL` to your Server URL/IP.

Start the porgram with
```
docker compose up -d
```

Shut it down with
```
docker compose down
```

To auto start the application everytime the Server start:
```
sudo cp firefly-iii.service.template /etc/systemd/system/firefly-iii.service
sudo nano /etc/systemd/system/firefly-iii.service
```
and set the `WorkingDirectory` to where you cloned this repository.
